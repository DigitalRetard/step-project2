//Слайдер
const thumbs = new Swiper(".thumbs-slider", {
    spaceBetween: 20,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesProgress: true,
});
const swiper = new Swiper(".slider", {
    spaceBetween: 20,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: thumbs,
    },
});

//Таби
function tabs () {
    const tabMenu = document.querySelector(".service-menu");
    tabMenu.addEventListener('click', event => {
        if (event.target.tagName ==='BUTTON') {
            change(event);
        }
    });
    const change = (event) => {
        let menuItems = document.querySelectorAll('[data-tab]');

        // for (const item of menuItems) {
        //     if (item.classList.contains('active')){
        //         item.classList.remove('active');
        //
        //     }
        // }
        if(!event.target.classList.contains('active')){
            event.target.classList.add('active');

        }
        let id = event.target.getAttribute('data-tab');
        let contentItems = document.querySelectorAll('.item-imag');
        for (const item of contentItems) {
            if (item.classList.contains('active')){
                item.classList.remove('active');

            }
        }
        if(!document.getElementById(id).classList.contains('active')){
            document.getElementById(id).classList.add('active');

        }
    }
}
tabs();
//Додавання ще 12 картинок і фільтр
function gallery() {
    let COUNT = 0;
    let COUTN_ITEMS = 12;
    let SET_TIMEOUT = 3000;
    let images = [
    {
        data: 'graphic_design',
        src: './images/img1.png'
    },{
        data: 'web_design',
        src: './images/img2.png'
    },{
        data: 'landing_pages',
        src: './images/img3.png'
    },{
        data: 'wordpress',
        src: './images/img4.png'
    },{
        data: 'graphic_design',
        src: './images/img5.png'
    },{
        data: 'web_design',
        src: './images/img6.png'
    },{
        data: 'landing_pages',
        src: './images/img7.png'
    },{
        data: 'wordpress',
        src: './images/img8.png'
    },{
        data: 'graphic_design',
        src: './images/img9.png'
    },{
        data: 'web_design',
        src: './images/img10.png'
    },{
        data: 'landing_pages',
        src: './images/img11.png'
    },{
        data: 'wordpress',
        src: './images/img12.png'
    },
]


    const loadMore = () => {
        const loadMore = document.querySelector('.load_more');

        loadMore.addEventListener('click', createImage)
    }

    const createImage = () => {
        const galleryContent = document.querySelector('.g-gallery .gallery-content');
        const loadMore = document.querySelector('.load_more');
        loadMore.classList.add('loader');

        const createArr = ([...source], maxLength) => Array.from(
            { length: Math.ceil(images.length / COUTN_ITEMS) },
            () => source.splice( 0, maxLength)
        );

        const imageArrays = createArr(images, COUTN_ITEMS);

        setTimeout(() => {
            if ( imageArrays.length >= COUNT+1 ) {
                for ( const image of imageArrays[COUNT] ) {
                    const div = document.createElement("div");
                    const imageTeg = document.createElement("img");
                    div.classList.add('image-item');
                    div.setAttribute('data-gallery', image.data);
                    imageTeg.setAttribute('src', image.src);
                    div.append(imageTeg);
                    galleryContent.append(div)
                }
            }

            COUNT++

            if ( COUNT === imageArrays.length ) {
                loadMore.remove()
            }
        }, SET_TIMEOUT);
    }


    loadMore()
    const listenerAll = () => {
        let menuItems = document.querySelectorAll('[data-menu]');

        for ( const element of menuItems) {
            element.addEventListener('click', change, false);
        }

    }

    const clear = () => {
        let menuElements = document.querySelectorAll('[data-menu]');

        for ( const element of menuElements) {
            element.classList.remove('active');
        }
    }

    const change = (e) => {
        clear();
        e.target.classList.add('active');
        let contentItems = document.querySelectorAll('[data-gallery]');
        let id = e.currentTarget.getAttribute('data-menu');

        for ( const content of contentItems) {
            if ( content.getAttribute('data-gallery') !== id && id !== 'all' ) {
                content.style.display = 'none';
            } else {
                content.style.display = 'block';
            }
        }
    }
    listenerAll()
}


document.addEventListener('DOMContentLoaded', () => {
    gallery();
});

